//
//  UIView+extension.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func centerToParentView(vertical: NSLayoutYAxisAnchor?, horizontal:NSLayoutXAxisAnchor?, size: CGSize = .zero ) {
        translatesAutoresizingMaskIntoConstraints = false
        if let vertical = vertical {
            centerYAnchor.constraint(equalTo: vertical).isActive = true
        }
        if let horizontal = horizontal {
            centerXAnchor.constraint(equalTo: horizontal).isActive = true
        }
    
        setSize(size: size)
    }
    
    func centerHorizontal(to anchor: NSLayoutXAxisAnchor, size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: anchor).isActive = true
        
        setSize(size: size)
    }

    func centerVertical(to anchor: NSLayoutYAxisAnchor, size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        centerYAnchor.constraint(equalTo: anchor).isActive = true
        
        setSize(size: size)
    }
    
    func aspectRatio(to view: UIView, multiplier: CGFloat = 1) {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: multiplier).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: multiplier).isActive = true
    }
    
    func setSize(size: CGSize) {
        translatesAutoresizingMaskIntoConstraints = false
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
    }
    
    func anchor(top: NSLayoutYAxisAnchor?, bottom: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, right: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let left = left {
            leadingAnchor.constraint(equalTo: left, constant: padding.left).isActive = true
        }
        
        if let right = right{
            trailingAnchor.constraint(equalTo:  right, constant: -padding.right).isActive = true
        }
            
        if let bottom = bottom{
            bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom).isActive = true
        }
        
        setSize(size: size);
    }
}
