//
//  UserModel.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import Foundation
import CoreData

class UserModel: Codable {
    var login: String?
    var id: Int?
    var node_id: String?
    var avatar_url: String?
    var gravatar_id: String?
    var url: String?
    var html_url: String?
    var followers_url: String?
    var following_url: String?
    var gists_url: String?
    var starred_url: String?
    var subscriptions_url: String?
    var organizations_url: String?
    var repos_url: String?
    var events_url: String?
    var type: String?
    var site_admin: Bool?
    
    // Profile Data
    var name:  String?
    var company: String?
    var blog: String?
    var location: String?
    var email: String?
    var hireable: String?
    var bio: String?
    var public_repos: Int?
    var public_gists: Int?
    var followers: Int?
    var following: Int?
    var created_at: String?
    var updated_at: String?
    
    var note: String?
    
    init() {}
    
    
    convenience init(user: NSManagedObject) {
        self.init()
        self.login = user.value(forKeyPath: "login") as? String
        self.id = user.value(forKeyPath: "id") as? Int
        self.node_id = user.value(forKeyPath: "nodeId") as? String
        self.avatar_url = user.value(forKeyPath: "avatar") as? String
        self.gravatar_id = user.value(forKeyPath: "gravatarId") as? String
        self.url = user.value(forKeyPath: "url") as? String
        self.html_url = user.value(forKeyPath: "htmlUrl") as? String
        self.followers_url = user.value(forKeyPath: "followersUrl") as? String
        self.following_url = user.value(forKeyPath: "followingUrl") as? String
        self.gists_url = user.value(forKeyPath: "gistsUrl") as? String
        self.starred_url = user.value(forKeyPath: "starredUrl") as? String
        self.subscriptions_url = user.value(forKeyPath: "subscriptionsUrl") as? String
        self.organizations_url = user.value(forKeyPath: "organizationsUrl") as? String
        self.repos_url = user.value(forKeyPath: "reposUrl") as? String
        self.events_url = user.value(forKeyPath: "eventsUrl") as? String
        self.type = user.value(forKeyPath: "type") as? String
        
        self.name = user.value(forKeyPath: "name") as? String
        self.company = user.value(forKeyPath: "company") as? String
        self.login = user.value(forKeyPath: "login") as? String
        self.blog = user.value(forKeyPath: "blog") as? String
        self.location = user.value(forKeyPath: "location") as? String
        self.email = user.value(forKeyPath: "email") as? String
        self.hireable = user.value(forKeyPath: "hireable") as? String
        self.bio = user.value(forKeyPath: "bio") as? String
        self.public_repos = user.value(forKeyPath: "publicRepos") as? Int
        self.public_gists = user.value(forKeyPath: "publicGists") as? Int
        self.followers = user.value(forKeyPath: "followers") as? Int
        self.following = user.value(forKeyPath: "following") as? Int
        self.created_at = user.value(forKeyPath: "createdAt") as? String
        self.updated_at = user.value(forKeyPath: "updatedAt") as? String
        self.note = user.value(forKeyPath: "note") as? String
    }
    
}
