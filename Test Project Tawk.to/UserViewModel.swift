//
//  UserViewModel.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import Foundation

struct UserViewModel {
    
    var name: String
    var followers: Int
    var followed: Int
    
    var site: String
    var location: String
    var company: String
    var email: String?
    
    var publicRepos: Int
    var publicGists: Int
    
    var avatar: String
    var username: String
    
    var note: String
    
    var id: Int
    
    var invertedImage = false
    
    init(userModel: UserModel) {
        name        = userModel.name ?? ""
        followers   = userModel.followers ??  0
        followed    = userModel.following ?? 0
        site        = userModel.blog ?? ""
        location    = userModel.location ?? ""
        publicRepos = userModel.public_repos ?? 0
        publicGists = userModel.public_gists ?? 0
        avatar      = userModel.avatar_url  ?? ""
        username    = userModel.login ?? ""
        id          = userModel.id ?? 0
        note        = userModel.note ?? ""
        company     = userModel.company ?? ""
        email       = userModel.email   ?? ""
    }
}
