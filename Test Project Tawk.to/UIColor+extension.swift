//
//  UIColor+extension.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public convenience init(hex6String: String, alpha: CGFloat = 1) {
        
        let hex6 = Int.init(hex6String, radix: 16) ?? 0x000000
        
        let divisor = CGFloat(255)
        let red     = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hex6 & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hex6 & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
        
    }
    
    static func colorWith(string: String) -> UIColor {
        return UIColor.init(hex6String: string)
    }
    
    
    static func textColor(traitCollection: UITraitCollection) -> UIColor? {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .white
        case
          .unspecified,
          .light: return .black
        default:
            return .black
        }
    }
    
    static func navigationCtrlBackground(traitCollection: UITraitCollection) -> UIColor? {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .systemGray2
        case
          .unspecified,
          .light: return .systemTeal
        default:
            return .black
        }
    }
    
    static func cellBackground(traitCollection: UITraitCollection) -> UIColor? {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .systemGray
        case
          .unspecified,
          .light: return .white
        default:
            return .black
        }
    }
    
    static func tableSeparator(traitCollection: UITraitCollection) -> UIColor? {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .white
        case
          .unspecified,
          .light: return .lightGray
        default:
            return .lightGray
        }
    }
    
    static func backgroundColor(traitCollection: UITraitCollection) -> UIColor {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .systemGray6
        case
          .unspecified,
          .light: return .white
        default:
            return .white
        }
    }
    
    static func defaultImageColor(traitCollection: UITraitCollection) -> UIColor {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .black
        case
          .unspecified,
          .light: return .white
        default:
            return .white
        }
    }
    
    static func defaultButtonTint(traitCollection: UITraitCollection) -> UIColor {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .white
        case
          .unspecified,
          .light: return .black
        default:
            return .black
        }
    }
    
    static func placeholderColor(traitCollection: UITraitCollection) -> UIColor {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .white
        case
          .unspecified,
          .light: return .lightGray
        default:
            return .lightGray
        }
    }
    
    static func textViewBackgroundColor(traitCollection: UITraitCollection) -> UIColor {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .white
        case
          .unspecified,
          .light: return .systemGray6
        default:
            return .systemGray6
        }
    }
    
    static func textViewTextColor(traitCollection: UITraitCollection) -> UIColor {
        switch traitCollection.userInterfaceStyle {
        case .dark: return .black
        case
          .unspecified,
          .light: return .black
        default:
            return .black
        }
    }
}
