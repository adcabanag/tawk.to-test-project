//
//  CoreDataManager.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import Foundation
import UIKit
import CoreData
class CoreDataManager {
    static let instance = CoreDataManager()
    
    func saveProfile(for userModel: UserModel) {
        if let user = fetch(userModel.id!) {
            user.setValue(userModel.name, forKeyPath: "name")
            user.setValue(userModel.company, forKeyPath: "company")
            user.setValue(userModel.blog, forKeyPath: "blog")
            user.setValue(userModel.location, forKeyPath: "location")
            user.setValue(userModel.email, forKeyPath: "email")
            user.setValue(userModel.email, forKeyPath: "hireable")
            user.setValue(userModel.bio, forKeyPath: "bio")
            user.setValue(userModel.public_repos, forKeyPath: "publicRepos")
            user.setValue(userModel.public_gists, forKeyPath: "publicGists")
            user.setValue(userModel.followers, forKeyPath: "followers")
            user.setValue(userModel.following, forKeyPath: "following")
            user.setValue(userModel.created_at, forKeyPath: "createdAt")
            user.setValue(userModel.created_at, forKeyPath: "createdAt")
            
            guard let appDelegate =
              UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    
    func saveUser(_ userModel: UserModel) {
        if fetch(userModel.id!) == nil {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
              return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "Users", in: managedContext)!
            
            let user = NSManagedObject(entity: entity, insertInto: managedContext)
            user.setValue(userModel.login, forKeyPath: "login")
            user.setValue(userModel.id, forKeyPath: "id")
            user.setValue(userModel.node_id, forKeyPath: "nodeId")
            user.setValue(userModel.avatar_url, forKeyPath: "avatar")
            user.setValue(userModel.gravatar_id, forKeyPath: "gravatarId")
            user.setValue(userModel.url, forKeyPath: "url")
            user.setValue(userModel.html_url, forKeyPath: "htmlUrl")
            user.setValue(userModel.followers_url, forKeyPath: "followersUrl")
            user.setValue(userModel.following_url, forKeyPath: "followingUrl")
            user.setValue(userModel.gists_url, forKeyPath: "gistsUrl")
            user.setValue(userModel.starred_url, forKeyPath: "starredUrl")
            user.setValue(userModel.subscriptions_url, forKeyPath: "subscriptionsUrl")
            user.setValue(userModel.organizations_url, forKeyPath: "organizationsUrl")
            user.setValue(userModel.repos_url, forKeyPath: "reposUrl")
            user.setValue(userModel.events_url, forKeyPath: "eventsUrl")
            user.setValue(userModel.type, forKeyPath: "type")
            user.setValue(userModel.site_admin, forKeyPath: "siteAdmin")

            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    func saveNote(_ userModel: UserViewModel){
        if let user = fetch(userModel.id) {
            user.setValue(userModel.note, forKeyPath: "note")
            
            guard let appDelegate =
              UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    func fetchNote(for userId: Int) -> String {
        if let user = fetchUser(userId) {
            return (user.note == nil ? "" : user.note)!
        }
        return ""
    }
    
    
    func fetchUser(_ userId: Int) -> UserModel? {
        if let user = fetch(userId)  {
            return UserModel(user: user)
        }
        return nil
    }
    
    func fetch(_ userId: Int) -> NSManagedObject? {
        let users = fetchUsers()
        
        for user in users {
            let id = user.value(forKeyPath: "id") as? Int
            if userId == id {
                return user
            }
        }
        return nil
    }
    
    func fetchUsers()  -> [NSManagedObject]{
        var users: [NSManagedObject] = []
        guard let appDelegate =
          UIApplication.shared.delegate as? AppDelegate else {
            return users
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Users")
        do {
          users = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
          print("Could not fetch. \(error), \(error.userInfo)")
        }
        return users
    }
    
    func fetchUserModelList() -> [UserModel] {
        var userModels = [UserModel]()
        let users = fetchUsers()
        for user in users {
            let userModel = UserModel(user: user)
            userModels.append(userModel)
        }
        return userModels
    }
}
