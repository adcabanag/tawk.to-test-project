//
//  UserListViewController.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import UIKit

class UserListViewController: UIViewController, ProfileUpdateDelegate{

    fileprivate var tableView = UITableView()
    fileprivate var refreshController = UIRefreshControl()
    fileprivate var paginationLoader: UIActivityIndicatorView?
    let userCellIdentifier = "UserCell"
    var userViewModels = [UserViewModel]()
    var loadPage = 0
    let userManager = UserManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "User List"
        view.backgroundColor = UIColor.backgroundColor(traitCollection: traitCollection)
        setupTableView()
        paginationLoader?.startAnimating()
        fetchList()
    }
    
    
    // MARK: UI MALIPULATION METHODS
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.separatorInset = .init(top: 0, left: 20, bottom: 0, right: 10)
        view.addSubview(tableView)
        tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 16, left: 0, bottom: 16, right: 0))
        
        tableView.register(UserCell.self, forCellReuseIdentifier: userCellIdentifier)
        
        
        refreshController.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshController.addTarget(self, action: #selector(refresh(_:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshController)
        
        paginationLoader = UIActivityIndicatorView(style: .large)
        paginationLoader?.color = UIColor.textColor(traitCollection: traitCollection)
        paginationLoader?.hidesWhenStopped = true
        paginationLoader?.isHidden = true
        tableView.tableFooterView = paginationLoader
    }
    
    @objc func refresh(_ sender: UIRefreshControl) {
        //Refresh list
        loadPage = 0
        userViewModels.removeAll()
        fetchList()
    }
    
    func popuplateListWithCoreData() {
        loadPage = 0
        userViewModels.removeAll()
        let userModels = CoreDataManager.instance.fetchUserModelList()
        userViewModels = userModels.map({ return UserViewModel(userModel: $0)})
        tableView.reloadData()
    }
    
    
    // MARK: API REQUESTS METHODS
    
    func fetchList() {
            self.userManager.getUserList(at: self.loadPage) { [weak self] (success, msg, userModelList) in
                if let wSelf = self {
                    DispatchQueue.main.async {
                        wSelf.refreshController.endRefreshing()
                        wSelf.paginationLoader?.stopAnimating()
                        if success {
                            for (index, userModel) in userModelList.enumerated() {
                                var user = UserViewModel(userModel: userModel)
                                user.note = CoreDataManager.instance.fetchNote(for: user.id)
                                user.invertedImage = (index+1) % 4 == 0
                                wSelf.userViewModels.append(user)
                            }
                            wSelf.tableView.reloadData()
                        } else {
                            wSelf.popuplateListWithCoreData()
                        }
                    }
                }
            }
    }
    
    
    func loadNextPage() {
            self.loadPage += 1
            self.tableView.tableFooterView = self.paginationLoader
            self.paginationLoader?.startAnimating()
            let previousUserViewModelsCount = self.userViewModels.count
            self.userManager.getUserList(at: self.loadPage) { [weak self] (success, msg, userModelList) in
                if let wSelf = self {
                    DispatchQueue.main.async {
                        wSelf.paginationLoader?.stopAnimating()
                        if success {
                            var newUserViewModels = [UserViewModel]()
                            for (index, userModel) in userModelList.enumerated() {
                                var user = UserViewModel(userModel: userModel)
                                user.note = CoreDataManager.instance.fetchNote(for: user.id)
                                user.invertedImage = (index+1) % 4 == 0
                                newUserViewModels.append(user)
                            }
                            wSelf.userViewModels.append(contentsOf: newUserViewModels)
                            var indexPaths = [IndexPath]()
                            for row in previousUserViewModelsCount...wSelf.userViewModels.count {
                                indexPaths.append(IndexPath(row: row, section: 0))
                            }
                            wSelf.tableView.reloadData()
                            wSelf.tableView.scrollToRow(at: IndexPath(row: previousUserViewModelsCount-1, section: 0), at: .bottom, animated: false)
                        } else {
                            wSelf.loadPage = wSelf.loadPage > 0 ? wSelf.loadPage-1 : 0
                        }
                    }
                }
            }
    }
    
    // MARK: DELEGATE METHODS
    func updateRow(indexPath: IndexPath) {
        self.tableView.reloadRows(at: [indexPath], with: .none)
    }
}


// MARK: TABLE METHODS
extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userViewModels.isEmpty {
            return 6
        } else {
            return userViewModels.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: userCellIdentifier , for: indexPath) as? UserCell {
            if userViewModels.isEmpty {cell.createPlaceholderCell() }
            else { cell.create(user:userViewModels[indexPath.row]) }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let profilePageVC = UserProfileViewController(nibName: "UserProfileViewController", bundle: Bundle.main)
            profilePageVC.userViewModel = userViewModels[indexPath.row]
            profilePageVC.delegate = self
            profilePageVC.indexPath = indexPath
            show(profilePageVC, sender: self)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
        if endScrolling >= scrollView.contentSize.height, !userViewModels.isEmpty  {
            self.loadNextPage()
        }
    }
    
    
}
