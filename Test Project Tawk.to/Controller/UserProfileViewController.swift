//
//  UserProfileViewController.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import UIKit
protocol ProfileUpdateDelegate {
    func updateRow(indexPath: IndexPath)
}

class UserProfileViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var profileImage: CachedImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var followed: UILabel!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var scrollViewBottomPadding: NSLayoutConstraint!
    var userViewModel: UserViewModel?
    var indexPath: IndexPath?
    var delegate: ProfileUpdateDelegate?
    
    
    override func viewDidLoad() {
           super.viewDidLoad()
           self.title = "Profile"
           view.backgroundColor = UIColor.backgroundColor(traitCollection: traitCollection)
        
           saveButton.setTitleColor(UIColor.defaultButtonTint(traitCollection: traitCollection), for: .normal)
           setPlaceHolderProfile()
           fetchUserProfile()
           
           let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapped(_:)))
           view.addGestureRecognizer(tapGesture)
           
           scrollView.contentSize = .init(width: UIScreen.main.bounds.width, height: 630)
           
           startKeyboardEventsObservers()
    }
    
    // MARK: UI MANIPULATION METHODS
    
    func populateUserProfile(userViewModel: UserViewModel) {
        if let url = URL(string: userViewModel.avatar) {
            profileImage.loadImageWithUrl(url)
        }
        
        name.text = userViewModel.name
        company.text = userViewModel.company
        location.text = userViewModel.location
        email.text = userViewModel.email
        followers.text = "\(userViewModel.followers) followers"
        followed.text = "\(userViewModel.followed) followed"
        notes.text = userViewModel.note
        notes.textColor = UIColor.textViewTextColor(traitCollection: traitCollection)
    }
    
    func removePlaceHolder() {
        profileImage.backgroundColor = .clear
        name.backgroundColor = .clear
        company.backgroundColor = .clear
        location.backgroundColor = .clear
        email.backgroundColor = .clear
        followers.backgroundColor = .clear
        followed.backgroundColor = .clear
        saveButton.setTitle("Save Note", for: .normal)
        saveButton.backgroundColor = .clear
        notes.backgroundColor = UIColor.textViewBackgroundColor(traitCollection: traitCollection)
    }
    
    func setPlaceHolderProfile() {
        profileImage.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        name.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        company.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        location.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        email.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        followers.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        followed.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        notes.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        saveButton.setTitle("", for: .normal)
        saveButton.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        
        name.text = ""
        company.text = ""
        location.text = ""
        email.text = ""
        followers.text = ""
        followed.text = ""
        notes.text = ""
    }
    
   
    
    
    // MARK: KEYBOARD OBSERVER METHODS
    
    private func startKeyboardEventsObservers() {
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow(_:)), name:UIResponder.keyboardWillShowNotification, object:nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide(_:)), name:UIResponder.keyboardWillHideNotification, object:nil)
    }

    private func stopObservingKeyboardEvents() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
      if let userInfo = notification.userInfo {
        let keyboardSize: CGSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size
        scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
      }
    }

    @objc func keyboardWillHide(_ notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero;
    }
    
    
    //MARK: API REQUEST METHODS

    func fetchUserProfile() {
            if let username = self.userViewModel?.username {
                UserManager.shared.getUserProfile(for: username)  { [weak self] (succes, msg, usermodel) in
                    if let wSelf = self {
                        if succes {
                            if !usermodel.isEmpty {
                                DispatchQueue.main.async {
                                    var userViewModel = UserViewModel(userModel: usermodel[0])
                                    userViewModel.note = CoreDataManager.instance.fetchNote(for: userViewModel.id)
                                    wSelf.removePlaceHolder()
                                    wSelf.populateUserProfile(userViewModel: userViewModel)
                                }
                            }
                        } else {
                            if let user = wSelf.userViewModel {
                                DispatchQueue.main.async {
                                    if let userModel = CoreDataManager.instance.fetchUser(user.id) {
                                        wSelf.removePlaceHolder()
                                        wSelf.populateUserProfile(userViewModel: UserViewModel(userModel: userModel))
                                    } else {
                                        wSelf.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    }
                }
            }
    }

    //MARK: USER INTERACTION METHODS
    
    @IBAction func saveNotes(_ sender: Any) {
        view.endEditing(true)
        if userViewModel != nil, let delegate = delegate, let index = indexPath{
            userViewModel?.note = self.notes.text
            CoreDataManager.instance.saveNote(userViewModel!)
            delegate.updateRow(indexPath: index)
        }
    }
    
    @objc func tapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

}
