//
//  UserManager.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import Foundation
import Network

typealias SuccessHandler = (_ success: Bool, _ msg: String?,_ userModels:[UserModel]) -> ()
struct FailableDecodable<Base : Decodable> : Decodable {

    let base: Base?

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.base = try? container.decode(Base.self)
    }
}
class UserManager {
    static let shared = UserManager()
    let userList = "https://api.github.com/users?since="
    let userProfile = "https://api.github.com/users/"
    let monitor = NWPathMonitor()
    init() {}
    
    func getUserList(at page: Int, completion: @escaping SuccessHandler){
        let url = URL(string: "\(self.userList)\(page)")!
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                completion(false,error?.localizedDescription, [])
            }
            if let data = data {
                do {
                    let userModels = try JSONDecoder().decode(Array<UserModel>.self, from: data)
                    for userModel in userModels {
                        DispatchQueue.main.async {
                            CoreDataManager.instance.saveUser(userModel)
                        }
                    }
                    completion(true,nil, userModels)
                } catch let error {
                    completion(false,error.localizedDescription, [])
                }
            }
    
        }).resume()
    }
    
    
    func getUserProfile(for username: String, completion: @escaping SuccessHandler) {
        let url = URL(string: "\(self.userProfile)\(username)")!
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                completion(false,error?.localizedDescription, [])
            }
            if let data = data {
                do {
                    let userModel = try JSONDecoder().decode(UserModel.self, from: data)
                    DispatchQueue.main.async {
                        if userModel.id != nil {
                            CoreDataManager.instance.saveProfile(for: userModel)
                        }
                    }
                    completion(true,nil, [userModel])
                } catch let error {
                    completion(false,error.localizedDescription, [])
                }
            }
        }).resume()
    }
    
}
