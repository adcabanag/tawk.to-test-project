//
//  UserCell.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import UIKit
class UserCell: UITableViewCell {
    func create(user: UserViewModel) {
        self.backgroundColor = .clear
        _ = subviews.map{ $0.removeFromSuperview()}
        let imageView = CachedImageView()
        imageView.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        if let url = URL(string: user.avatar ) {
            imageView.loadImageWithUrl(url, inverted: user.invertedImage)
        }
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        
        addSubview(imageView)
        imageView.anchor(top: nil, bottom: nil, left: self.leadingAnchor , right: .none, padding: .init(top: 0, left: 16, bottom: 0, right: 0), size: .init(width: 60, height: 60))
        imageView.centerVertical(to: self.centerYAnchor)

        let noteIcon = UIImageView()
        noteIcon.image = UIImage(named: "note_icon")
        noteIcon.tintColor = UIColor.textColor(traitCollection: traitCollection)
        addSubview(noteIcon)
        noteIcon.anchor(top: nil, bottom: nil, left: nil, right: self.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 16), size: .init(width: 30, height: 30))
        noteIcon.centerVertical(to: imageView.centerYAnchor)
        noteIcon.isHidden = CoreDataManager.instance.fetchNote(for: user.id).isEmpty
        
        let userLabel = UILabel()
        userLabel.text = user.username
        addSubview(userLabel)
        userLabel.font = UIFont.systemFont(ofSize: 18)
        userLabel.anchor(top: nil, bottom: nil, left: imageView.trailingAnchor, right: noteIcon.leadingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        userLabel.centerVertical(to: imageView.centerYAnchor)
        userLabel.setSize(size: .init(width: 0, height: 20))
        
        addSeparator()
            
    }
    
    func addSeparator() {
        let separator = UIView()
        separator.backgroundColor = UIColor.tableSeparator(traitCollection: traitCollection)
        addSubview(separator)
        separator.anchor(top: nil, bottom: self.bottomAnchor, left: self.leadingAnchor, right: self.trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 10), size: .init(width: 0, height: 1))
    }
    
    func createPlaceholderCell() {
        self.backgroundColor = .clear
        _ = subviews.map{ $0.removeFromSuperview()}
        let imagePlaceholder = UIView()
        imagePlaceholder.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        imagePlaceholder.layer.cornerRadius = 25
        imagePlaceholder.layer.masksToBounds = true
        addSubview(imagePlaceholder)
        imagePlaceholder.anchor(top: nil, bottom: nil, left: self.leadingAnchor , right: .none, padding: .init(top: 0, left: 16, bottom: 0, right: 0), size: .init(width: 60, height: 60))
        imagePlaceholder.centerVertical(to: self.centerYAnchor)
        
        let noteIconPlaceHolder = UIView()
        noteIconPlaceHolder.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        addSubview(noteIconPlaceHolder)
        noteIconPlaceHolder.anchor(top: nil, bottom: nil, left: nil, right: self.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 16), size: .init(width: 30, height: 30))
        noteIconPlaceHolder.centerVertical(to: imagePlaceholder.centerYAnchor)
        
        let userPlaceholder = UIView()
        userPlaceholder.layer.cornerRadius = 10
        userPlaceholder.layer.masksToBounds = true
        userPlaceholder.backgroundColor = UIColor.placeholderColor(traitCollection: traitCollection)
        addSubview(userPlaceholder)
        userPlaceholder.anchor(top: nil, bottom: nil, left: imagePlaceholder.trailingAnchor, right: noteIconPlaceHolder.leadingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        userPlaceholder.centerVertical(to: imagePlaceholder.centerYAnchor)
        userPlaceholder.setSize(size: .init(width: 0, height: 20))
        
        addSeparator()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
