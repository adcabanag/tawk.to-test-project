//
//  CachedImageView.swift
//  Test Project Tawk.to
//
//  Created by Ariel Dominic Cabanban Cabanag on 4/6/20.
//  Copyright © 2020 Ariel Dominic Cabanban Cabanag. All rights reserved.
//

import Foundation
import UIKit
let imageCache = NSCache<AnyObject, AnyObject>()
class CachedImageView: UIImageView {

    var imageURL: URL?
    var inverted: Bool = false

    func loadImageWithUrl(_ url: URL, inverted: Bool = false) {

        imageURL = url
        self.inverted = inverted
        image = nil

        // retrieves image if already available in cache
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {

            self.image = filterImage(image: imageFromCache)
            
            return
        }

        // image does not available in cache.. so retrieving it from url...
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in

            if error != nil {
                print(error as Any)
                return
            }

            DispatchQueue.main.async(execute: {

                if let unwrappedData = data, let imageToCache = UIImage(data: unwrappedData) {

                    if self.imageURL == url {
                        self.image = self.filterImage(image: imageToCache)
                    }

                    imageCache.setObject(imageToCache, forKey: url as AnyObject)
                }
            })
        }).resume()
    }
    
    func filterImage(image:UIImage) -> UIImage {
        if !inverted {
            return image
        }
        
        let beginImage = CIImage(image: image)
        if let filter = CIFilter(name: "CIColorInvert") {
            filter.setValue(beginImage, forKey: kCIInputImageKey)
            let newImage = UIImage(ciImage: filter.outputImage!)
            return newImage
        }
        return image
    }
}
